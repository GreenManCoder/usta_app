from django.urls import path 
from . import views

app_name = 'app'
urlpatterns = [

#----------------------------------------------login ----------------------------------------------------------------------------------------------------------
    path('',views.form_login, name= 'form_login'),
    path('iniciar_sesion/', views.iniciar_sesion, name="iniciar_sesion"),
    path('logout/', views.cerrar_sesion, name='cerrar_sesion'),
    path('emergencia/', views.emergencia , name = 'emergencia'),
#-------------------------------------------estudiante -------------------------------------------------------------------------------------------------------
    
    path('estudiante/', views.estudiante , name = 'estudiante'),
    path('estudiante_ver_lista_grupos/', views.estudiante_ver_lista_grupos , name = 'estudiante_ver_lista_grupos'),
    path('estudiante_ver_grupo/', views.estudiante_ver_grupo , name = 'estudiante_ver_grupo'),
    path('estudiante_ver_grupo/<int:id>/', views.materia_view, name='clases'),
#-------------------------------------------administrador -----------------------------------------------------------------------------------------------------
    path('administrador/', views.administrador , name = 'administrador'),
    path('docentes/listado', views.lista_docentes , name = 'lista_docentes'),
    path('grupos/crear/', views.form_crear_grupos , name = 'form_crear_grupos'),
    path('grupos/crear_post/', views.crear_grupo_post , name = 'crear_grupo'),
    path('grupos/editar/<int:id>/', views.form_editar_grupo_admi , name = 'form_editar_grupo_admi'),
    path('grupos/', views.ver_lista_grupos , name = 'ver_lista_grupos'),
    path('grupos/eliminar/<int:id>/', views.eliminar_grupos_admin , name = 'eliminar_grupos_admin'),
    path('grupos/eliminar_confirm/<int:id>/', views.eliminar_grupos_admin_post , name = 'eliminar_grupos_admin_post'),
    path('grupos/agregar_estudiante/<int:id>/', views.form_agregar_estudiante_grupo , name = 'form_agregar_estudiante_grupo'),
    path('docente/crear/', views.form_crear_profesor , name = 'form_crear_profesor'),
    path('docente/crear_post/', views.crear_profesor_post , name = 'crear_profesor'),
    path('grupos/ver_grupo/<int:id>/', views.ver_grupo_admin , name = 'ver_grupo_admin'),
    path('crear_estudiante/', views.form_crear_estudiante , name = 'form_crear_estudiante'),
    path('listado_estudiantes/', views.listado_estudiantes , name = 'listado_estudiantes'),
    path('editar_estudiantes/', views.editar_estudiantes , name = 'editar_estudiantes'),
    path('ver_profesor/', views.ver_profesor , name = 'ver_profesor'),
    path('lista_grupos/', views.lista_grupos, name = 'listadoGrupos'),
#-------------------------------------------- docente ----------------------------------------------------------------------------------------------------------
    path('docente/', views.docente , name = 'docente'),
    path('profe_crear_actividad/', views.profe_crear_actividad, name = 'profe_crear_actividad'),
    path('profe_crear_actividad/<int:id>/', views.crear_view, name='materia'),
    path('crear_view/', views.crear_actividad_post , name = 'crear_actividad'),
    path('profe_editar_actividad/', views.profe_editar_actividad, name = 'profe_editar_actividad'),
    path('profe_editar_actividad/<int:id>/', views.editar_view, name='editar'),
    path('profe_ver_actividad/', views.profe_ver_actividad , name = 'profe_ver_actividad'),
    path('profe_ver_actividad/<int:id>/', views.ver_actividad, name='ver'),
    path('profe_calificar_actividad/', views.profe_calificar_actividad , name = 'profe_calificar_actividad'),
    path('profe_calificar_actividad/<int:id>/', views.calificar_actividad , name = 'calificar'),
    path('calificar_actividad/', views.calificar_actividad_post , name = 'calificar_actividad'),
    path('profe_verlista_grupos/', views.profe_verlista_grupos , name = 'profe_verlista_grupos'),
    path('profe_ver_lista_grupo/', views.profe_ver_lista_grupo , name = 'profe_ver_lista_grupo'),
    path('profe_ver_lista_grupo/<int:id>/', views.View_lista_alumnos , name = 'lista'),
    path('profe_ver_estudiante/', views.profe_ver_estudiante , name = 'profe_ver_estudiante'),
    path('profe_ver_estudiante/<int:id>/', views.View_lista_alumnos2 , name = 'lista2'),
]