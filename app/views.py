from  django.shortcuts  import render 
from django.http import HttpResponse, request
from app.models import Estudiante, Grupo, Actividad, EstudianteGrupo, Nota, Profesor
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.shortcuts import get_object_or_404

#------------------------------------------------------EMPIEZA EL LOGIN ---------------------------------------------------------
def form_login(request):
    return render(request, 'app/login.html')
    
def iniciar_sesion(request):
    u = request.POST['username']
    p = request.POST['password']

    usuario = authenticate(username=u, password=p)

    if usuario is not None:
        login(request, usuario)
        
        estudiantes = Estudiante.objects.filter(user_id=usuario.id)
        docentes = Profesor.objects.filter(user_id=usuario.id)
        if len(estudiantes) > 0:
            return redirect('app:estudiante')
        elif len(docentes) > 0:
            return redirect('app:docente')
        else:
            return redirect('app:administrador')
    else:
        contexto = {
            'error': 'Credenciales no válidas'
        }
        return render(request, 'app/login.html', contexto)

def cerrar_sesion(request):
    logout(request)
    return redirect('app:form_login')

def emergencia(request):
    return render(request, 'app/index3.html')
#------------------------------------------------------TERMINA EL LOGIN -----------------------------------------------------------

#-------------------------------------------------------EMPIEZA ESTUDIANTE ------------------------------------------------------
@login_required
def estudiante(request):
    return render(request, 'app/estudiante.html')

def estudiante_ver_lista_grupos(request):
    estudiante = EstudianteGrupo.objects.all()
    grupo = Grupo.objects.all()
    contexto = {
        'materias': estudiante,
        'grupo':grupo
    }
    return render(request, 'app/estudiante_ver_lista_grupos.html',contexto)

def estudiante_ver_grupo(request):
    lista_materias = EstudianteGrupo.objects.all()
    contexto = {
        'materias': lista_materias,   
    }
    return render(request, 'app/estudiante_ver_grupo.html',contexto)

def materia_view(request,id):
    clases = EstudianteGrupo.objects.get(id=id)
    actividad = Actividad.objects.all()
    contexto = {
        'clases': clases,
        'actividad':actividad,
    }
    return render(request,'app/estudiante_ver_materia.html',contexto)
#-------------------------------------------------------TERMINA ESTUDIANTE ------------------------------------------------------

#-------------------------------------------------------EMPIEZA DOCENTE ---------------------------------------------------------
@login_required
def docente(request):
    return render(request, 'app/docente.html')

def profe_crear_actividad(request):
    lista_materias = Grupo.objects.all()
    contexto = {
        'materias': lista_materias
    }
    return render(request,'app/profe_crear_actividad.html',contexto)

def crear_view(request,id):
    materia = Grupo.objects.get(id=id)
    clase = Grupo.objects.all()
    actividad = Actividad.objects.all()
    contexto = {
        'materia': materia,
        'actividad':actividad,
        'clase':clase
    }
    return render(request,'app/form_crear_actividad.html',contexto)

def crear_actividad_post(request):  
    descripcion = request.POST['descripcion']
    categoria = request.POST['categoria']
    categoria = Grupo.objects.get(id=categoria)
    acti = Actividad()
    acti.descripcion= descripcion
    acti.grupo = categoria
    acti.save()
    return redirect('app:profe_crear_actividad')

def profe_editar_actividad(request):
    lista_materias = Grupo.objects.all()
    contexto = {
        'materias': lista_materias
    }
    return render(request, 'app/profe_editar_actividad.html',contexto)

def editar_view(request,id):
    materia1 = Grupo.objects.get(id=id)
    clase1 = Grupo.objects.all()
    actividad1 = Actividad.objects.all()
    contexto = {
        'materia1': materia1,
        'actividad1':actividad1,
        'clase1':clase1
    }
    return render(request,'app/form_editar_view.html',contexto)

def editar_actividad_post(request):
    return redirect ('app:profe_editar_actividad')

def profe_ver_actividad(request):
    lista_materias = Grupo.objects.all()
    contexto = {
        'materias': lista_materias,
    }
    return render(request, 'app/profe_ver_actividad.html',contexto)

def ver_actividad(request,id):
    materia = Grupo.objects.get(id=id)
    contexto = {
        'materia': materia,
    }
    return render(request,'app/ver_actividad.html',contexto)

def profe_calificar_actividad(request):
    lista_materias = Grupo.objects.all()
    contexto = {
        'materias2': lista_materias
    }
    return render(request, 'app/profe_calificar_actividad.html',contexto)

def calificar_actividad(request, id):
    materias3 = Grupo.objects.get(id=id)
    estudiante_grupo = Estudiante.objects.all()
    actividades = Actividad.objects.all()
    contexto = {
        'materias3': materias3,
        'estudiante_grupo':estudiante_grupo,
        'actividades':actividades
    }
    return render(request, 'app/calificar_actividad.html',contexto)

def calificar_actividad_post(request):
    return redirect('app:profe_calificar_actividad')

def profe_verlista_grupos(request):
    lista_materias = Grupo.objects.all()
    contexto = {
        'materias2': lista_materias
    }
    return render(request, 'app/profe_verlista_grupos.html',contexto)

def profe_ver_lista_grupo(request):
    lista_materias = Grupo.objects.all()
    contexto = {
        'materias': lista_materias
    }
    return render(request, 'app/profe_ver_lista_grupo.html',contexto)

def View_lista_alumnos(request, id):
    materia = Grupo.objects.get(id=id)
    contexto = {
        'materia': materia,
    }
    return render (request, 'app/lista_grupo.html',contexto)

def profe_ver_estudiante(request):
    lista_materias = Grupo.objects.all()
    contexto = {
        'materias': lista_materias
    }
    return render(request, 'app/profe_ver_estudiante.html',contexto)

def View_lista_alumnos2(request, id):
    materia = Grupo.objects.get(id=id)
    clase = Grupo.objects.all()
    actividad = Actividad.objects.all()
    contexto = {
        'materia': materia,
        'actividad':actividad,
        'clase':clase
    }
    return render (request, 'app/lista_grupo2.html',contexto)

#------------------------------------------------------TERMINA DOCENTE ------------------------------------------------------------

#------------------------------------------------------EMPIEZA ADMINISTRADOR -----------------------------------------------------------
@login_required
def administrador(request):
    return render(request, 'app/administrador.html')

def form_crear_grupos(request):
    profesor = Profesor.objects.all()

    contexto = {
        'profesor': profesor
    }
    return render(request, 'app/formCrearGrupos.html', contexto)

def crear_grupo_post(request):
    codigo = request.POST['codigo']
    asignatura = request.POST['asignatura']
    semestre = request.POST['semestre']
    id_profesor = request.POST['profesores']
    profesor = Profesor.objects.get(user_id=id_profesor)
    
    grupo = Grupo()
    grupo.codigo = codigo
    grupo.asignatura = asignatura
    grupo.semestre = semestre
    grupo.profesor = profesor
    grupo.save()
    return redirect('app:ver_lista_grupos')

def form_editar_grupo_admi(request, id):
    grupo = Grupo.objects.get(id=id)
    
    contexto = {
        'grupo': grupo
    }
    return render(request, 'app/editarGrupoAdmin.html',contexto)

def editar_grupo_post(request, id):
    codigo = request.POST['codigo']
    asignatura = request.POST['asignatura']
    semestre = request.POST['semestre']
    id_profesor = request.POST['profesores']
    profesor = Profesor.objects.get(user_id=id_profesor)
    
    grupo = Grupo()
    grupo.codigo = codigo
    grupo.asignatura = asignatura
    grupo.semestre = semestre
    grupo.profesor = profesor
    grupo.save()
    return redirect('app:ver_lista_grupos')

def ver_lista_grupos(request):
    groups_list = Grupo.objects.all()
    contexto = {
        'groups' : groups_list
    }
    return render(request, 'app/verListaGruposAdmi.html', contexto)

def ver_grupo_admin(request, id):
    grupo = Grupo.objects.get(id=id)
    contexto = {
        'grupo' : grupo
    }
    return render(request, 'app/verGrupoadmin.html', contexto)

def eliminar_grupos_admin(request, id):
    grupo = Grupo.objects.get(id=id)
    contexto = {
        'grupo' : grupo
    }
    return render(request, 'app/eliminarGrupoAdmin.html', contexto)

def eliminar_grupos_admin_post(request, id):
    grupo = Grupo.objects.get(id=id)
    grupo.delete()
    contexto = {
        'grupo' : grupo
    }
    return render(request, 'app/verListaGruposAdmi.html', contexto)

def form_agregar_estudiante_grupo(request, id):
    grupo =  Grupo.objects.get(id=id)
    estudiantes = Estudiante.objects.all()
    contexto = {
        'grupo': grupo,
        'estudiantes': estudiantes
    }
    return render(request, 'app/formAgregarEstudiante.html', contexto)

''' def agregar_estudiante_grupo_post(request):
    codigo = request.POST['codigo']
    asignatura = request.POST['asignatura']
    semestre = request.POST['semestre']
    id_profesor = request.POST['profesores']
    profesor = Profesor.objects.get(user_id=id_profesor)
    
    grupo = Grupo()
    grupo.codigo = codigo
    grupo.asignatura = asignatura
    grupo.semestre = semestre
    grupo.profesor = profesor
    grupo.save()
    return redirect('app:ver_lista_grupos') '''

def form_crear_profesor(request):

    return render(request, 'app/formCrearProfesor.html')

def crear_profesor_post(request):
    apellidos = request.POST['apellidos']
    nombres = request.POST['nombres']
    email = request.POST['email']
    catedra = request.POST['catedra']
    password =request.POST['password']

    profesor = User()
    profesor.last_name = apellidos
    profesor.first_name = nombres
    profesor.email = email
    profesor.set_password('123')
    profesor.save()

    return redirect('app:lista_docentes')

def lista_docentes(request):
    docentes = User.objects.all()

    contexto = {
        'docentes': docentes
    }
    return render(request, 'app/verListaDocentes.html',contexto)

def ver_profesor(request):
    return render(request, 'app/verProfesor.html')

def form_crear_estudiante(request):
    return render(request, 'app/formCrearEstudiante.html')

def listado_estudiantes(request):
    
    return render(request, 'app/ver_lista_estudiantes.html')

def editar_estudiantes(request):
    return render(request, 'app/editar_estudiantes.html')


def lista_grupos(request):
    lista_grupos1 = {
        'listado_grupos1': [
            {'semestre': 1, 'codigo': 2218979, 'asignatura': 'Soluciones Nube', 'cantidad_estu': 11},            
            {'semestre': 2,'codigo': 2218970, 'asignatura': 'Matematicas', 'cantidad_estu': 10},
            {'semestre': 2, 'codigo': 2218971, 'asignatura': 'Fisica', 'cantidad_estu': 9}
        ],
    }
    return render(request, 'app/listaCursos.html', lista_grupos1)

#------------------------------------------------------TERMINA ADMINISTRADOR -----------------------------------------------------------