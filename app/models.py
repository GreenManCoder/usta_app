from django.db import models
from django.contrib.auth.models import User

class Profesor(models.Model):
    catedra = models.BooleanField(null=False)
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        primary_key=True,
    )

    fecha_creacion = models.DateTimeField(auto_now_add=True)

    class Meta: 
        app_label = 'app'
    


class Estudiante(models.Model):
    codigo = models.CharField(max_length=20, null= False)
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        primary_key=True,
    )
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    
    class Meta: 
        app_label = 'app'


class Grupo(models.Model):
    codigo = models.CharField(max_length=50, null= False)
    asignatura = models.CharField(max_length=100, null= False)
    semestre = models.CharField(max_length=20, null= False)
    profesor = models.ForeignKey(Profesor, null=True ,related_name='profesores',on_delete=models.PROTECT)
    
    class Meta: 
        app_label = 'app'


class EstudianteGrupo(models.Model):
    grupo = models.ForeignKey(Grupo, null=True,related_name='grupos',on_delete=models.PROTECT)
    estudiante = models.ForeignKey(Estudiante, null=True,related_name='estudiantes',on_delete=models.PROTECT)
    
    class Meta: 
        app_label = 'app'


class Actividad(models.Model):
    descripcion = models.CharField(max_length=100, null=False)
    grupo = models.ForeignKey(Grupo, null=True,related_name='actividades',on_delete=models.PROTECT)

    class Meta: 
        app_label = 'app'

class Nota(models.Model):
    estudiante_grupo = models.ForeignKey(EstudianteGrupo, null=True,related_name='notas',on_delete=models.PROTECT)
    actividad = models.ForeignKey(Actividad, null=True,related_name='evaluaciones',on_delete=models.PROTECT)
    valor = models.FloatField(null= False)

    class Meta: 
        app_label = 'app'